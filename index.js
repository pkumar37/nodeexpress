let express = require('express');
let bodyParser = require('body-parser');
//import bodyParser from 'body-parser' ;
let routers = require('./routes/users.js');
const app = express();
const PORT = 5000;
//
app.use(bodyParser.json());
app.use('/users', routers);
app.get('/', (req, res) => {
    res.send('Hello from Index page')
})
//app.listen(PORT);
app.listen(PORT, () => console.log(`Server is running on port: http://localhost:${PORT}`));