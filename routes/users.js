const express = require('express');
const router = express.Router();
const { v4: uuidv4 } = require('uuid'); // for Uniuq Id generation


const users2 = []
router.get('/getUserDetails', (req, res) => {
    res.send(users2);
});

router.post('/addUserDetails', (req, res) => {
    const user = req.body;
    users2.push({ ...user, id: uuidv4() });
    res.send(`User with name:${user.firstName} added to DB successfully.`);
})

router.get('/getUserDetails/:id', (req, res) => {
    const { id } = req.params;
    console.log('users2:::', users2, id)
    const founuser = users2.find((user) => user.id == id);
    console.log('founuser::', founuser)
    res.send(founuser);
});

module.exports = router;
//module.exports = testData;
